Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ibus-table-chinese
Source: https://github.com/mike-fabian/ibus-table-chinese
Comment:
 This file needs further update.

Files: *
Copyright:
 Yu Yuwei (acevery / 余钰炜) <acevery at gmail.com>
 Caius 'kaio' Chance <k at kaio.me>
 Ding-Yi Chen (陳定彞) <dchen at redhat.com>
 Mike FABIAN <maiku.fabian@gmail.com>
License: GPL-3.0-only
Comment:
 List from AUTHORS file.

Files: debian/*
Copyright: 2010 Asias He <asias@debian.org>
License: GPL-3.0-only

Files: metainfo/*.appdata.xml
Copyright: 2022-2023 Mike FABIAN <maiku.fabian@gmail.com>
License: CC0-1.0

Files:
 tables/array/*
Copyright: 葉光哲, 廖明德
License: custom-Freely-redistributable-without-restriction
Comment:
 See the file header of array30.head.in.

Files:
 tables/cangjie/cangjie-big.txt
Copyright:
 * CHU Bong-Foo (朱邦復) http://www.cbflabs.com
 Roy Hiu-yueng Chan, ChineseCJ (倉頡之友馬來西亞) http://www.chinesecj.com
 Mak Che-Hung (麥志洪) <makchehu at netvigator.com>
 Quintus Leung <quintus at scj2000.net>
 Wen-Yen Chuang (Caleb) <caleb at calno.com>
 Matthew Kee <kocch at msn.com>
 Leung King Man (梁敬文) <leungkm1 at hotmail.com>, Hong Kong Seniors IT Advocates (香港長者資訊天地) <info at hkseniors.net> 
License: GPL-2.0-only
Comment:
 See the AUTHORS file and table file headers.

Files:
 tables/cangjie/cangjie3.txt
 tables/cangjie/cangjie5.txt
Copyright:
 chinesecj.com (倉頡之友‧馬來西亞)
 Roy Hiu-yueng Chan
 2008 Caius Chance <cchance AT redhat DOT com>
 2008 Yu Yuwei <acevery@gmail.com>
 2008 Wen-Yen Chuang (Caleb) <caleb AT calno DOT com>
License: custom-Freely-redistributable-without-restriction 
Comment:
 See the file headers.

Files:
 tables/cangjie/improve_cangjie5.py
Copyright:
 2021 Mike FABIAN <mfabian@redhat.com>
License: GPL-3.0-or-later

Files:
 tables/cantonese/cantonese.txt
Copyright:
 Matthew LEE <kocch@msn.com>
License: MIT

Files:
 tables/cantonese/cantonhk.txt
Copyright:
 香港長者資訊天地 Hong Kong Seniors IT Advocates <info at hkseniors.net>
 梁敬文 Leung King Man <leungkm1 at hotmail.com>
License: GPL-3.0-or-later
Comment:
 http://www.hkseniors.net/input/canton/
 .
 Also see file headers.

Files:
 tables/cantonese/cantonyale.txt
Copyright:
 xiota (https://github.com/xiota)
License: GPL-2.0-only
Comment: See file headers and file git history.

Files:
 tables/cantonese/improve_jyutping.py
Copyright:
 2022 Mike FABIAN <mfabian@redhat.com>
License: GPL-3.0-or-later

Files:
 tables/cantonese/jyutping.txt
Copyright:
 Caius 'kaio' Chance <k at kaio.me>
License: GPL-2.0-only
Comment: See git commit history.

Files:
 tables/cantonese/unicode_to_jyutping_table.pl
Copyright:
 2017 Anthony Wong
License: GPL-2.0-or-later

Files:
 tables/easy/easy-big.txt
Copyright:
 2005 Woodman Tuen <wmtuen@gmail.com>
License: GPL-2.0-or-later
Comment: See file headers ("GPL V2.0 或更新" -> GPL-2.0-or-later).

License: CC0-1.0
 On Debian systems, the full text of the CC0 1.0 Universal License
 can be found in the file `/usr/share/common-licenses/CC0-1.0'.

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of GPL-2 could be found at
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of GPL-3 could be found at
 `/usr/share/common-licenses/GPL-3'.

License: GPL-3.0-only
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-2.0-only
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".
